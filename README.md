Tribute-ScottCunningham

Tribute to Wiccan author Scott Cunningham
--------------------------------------------------------

A tribute to the late American author, Scott Cunningham, who opened up Wiccan philosophies to those hoping to explore their spirituality and connection to the natural world. 
Responsive HTML5, CSS3, SASS and Bootstrap for FCC (FreeCodeCamp)

Site URL: http://phpsitescripts.com/repos/tribute-scottcunningham/

CodePen URL: http://codepen.io/PHPSiteScripts/pen/zoPGGm

Github: https://github.com/SabrinaMarkon/Tribute-ScottCunningham
